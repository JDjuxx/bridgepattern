

open class Controler{
    protected open lateinit var person: Person

    constructor(person: Person){
        this.person = person
    }

    constructor()

    fun enamorar(){
        var loveLevel = person.getLoveLevel();
        person.setLoveLevel(loveLevel + 20)
    }

    fun pedirRegalo(){
        if(person.getCash() > 40){
            println("Te gustaría ir a una fiesta.. Pero tú me invitas la entrada")
        }else{
            println("Me compras el almuerzo, hoy no traje :c")
        }
    }

    fun fortalecerAmistad(){
        var friendshipLevel = person.getFriendshipLevel()
        person.setFriendshipLevel(friendshipLevel + 20)
    }
}

class AdvanceControler(person: Person) : Controler(){
    override var person:Person = person
    fun cambiarTema(){
        if(person.isAngry()){
            println("Ya olvidalo mejor acompañame a un evento")
        }else{
            println("Te quiero mucho amig@ :3")
        }

    }
}


interface Person{
    fun setLoveLevel(loveLevel: Int)
    fun getLoveLevel():Int
    fun isAngry():Boolean
    fun getCash():Int
    fun getFriendshipLevel():Int
    fun setFriendshipLevel(friendshipLevel: Int)
}

class JuanDiego : Person{
    private var loveLevel:Int = 40
    private var friendshipLevel:Int = 50
    private var isAngry:Boolean = false
    private var cash:Int = 90
    override fun getFriendshipLevel(): Int {
        return this.friendshipLevel
    }

    override fun setFriendshipLevel(friendshipLevel:Int) {
        this.friendshipLevel = friendshipLevel

    }

    override fun getCash(): Int {
        return this.cash
    }

    override fun isAngry(): Boolean {
        return this.isAngry
    }

    override fun getLoveLevel(): Int {
        return this.loveLevel
    }

    override fun setLoveLevel(loveLevel: Int){
        this.loveLevel = loveLevel
    }
}

class Andrea : Person{
    private var loveLevel:Int = 70
    private var friendshipLevel:Int = 30
    private var isAngry:Boolean = true
    private var cash:Int = 10
    override fun getFriendshipLevel(): Int {
        return this.friendshipLevel
    }

    override fun setFriendshipLevel(friendshipLevel:Int) {
        this.friendshipLevel = friendshipLevel

    }

    override fun getCash(): Int {
        return this.cash
    }

    override fun isAngry(): Boolean {
        return this.isAngry
    }

    override fun getLoveLevel(): Int {
        return this.loveLevel
    }

    override fun setLoveLevel(loveLevel: Int){
        this.loveLevel = loveLevel
    }
}

fun main(args: Array<String>) {
    var frienzoneado = JuanDiego()
    var girlcontroler = Controler(frienzoneado)
    println("El primer frienzoneado sera Juan Diego un chico muy inocente que solo quiere el amor de Sammy")
    println("Sammy le pedira un regalo a juan dependiendo de su situacion economica actual")
    girlcontroler.pedirRegalo()
    if(frienzoneado.getLoveLevel() < 60){
        println("El nivel de amor con Juan Diego es: "+frienzoneado.getLoveLevel())
        girlcontroler.enamorar()
        println("Usando tacticas de coqueteo y engaño.......")
        println("Ahora Juan Diego sigue enamorado ilusamente ;)")
    }
    println("\n")
    var frienzoneado2 = Andrea()
    var boycontroler = Controler(frienzoneado2)
    println("La segunda friendzoneada es Andrea, ella es una chica que siempre estuvo enamorada de Jhonny")
    if(frienzoneado2.getFriendshipLevel() < 50){
        println("El nivel de amistad com Andrea es: "+frienzoneado2.getFriendshipLevel())
        boycontroler.fortalecerAmistad()
        println("Mostrando falso interés en la amistad........")
        println("La amistad se restableció y confudió más a Andrea")
    }
    var boycontroler2 = AdvanceControler(frienzoneado2)
    println("Andrea está enojada y Jhonny no se quiere disculpar entonces.....")
    boycontroler2.cambiarTema()

}

